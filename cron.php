<?php

function autoLoader($name) {
    require 'classes/'.$name.'.php';
}

spl_autoload_register('autoLoader');

require_once  __DIR__ . '/config.php';
require_once __DIR__ . '/lib/phpQuery/phpQuery.php';

$logger = new LogWrite();
$logger->logWriter("Начало выполнения скрипта");

if (!empty($cityNames)){
    foreach ($cityNames as $cityName){
        $controller = new MainController($cityName,$csv_folder,$output);
        $controller->worker();
        unset($controller);
    }
}

$logger->logWriter("Конец выполнения скрипта\r\n");


