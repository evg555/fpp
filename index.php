<?php
//Так как я всю ночь не спал...
function ClearArchiveByMonth($array, $month){
    $intMonth = date('m', strtotime($month));

    foreach ($array as $key => $value)
    {
        if(!preg_match("@[0-9]{2}-".$intMonth."-[0-9]+@is", "farpost-14-09-12_01.csv"))
        {
            unset($array[$key]);
        }
    }
    return $array;
}


$folder = "lists/";

//Удаляем файлв с точками
$files = array_diff(scandir($folder,true), array('..', '.'));
foreach ($files as $file){
    if (preg_match('/^\.(.*)/',$file))  continue;
    $dir[] = $file;
}

$month = isset($_GET['month']) ? $_GET['month'] : 'none';

if($month != 'none')
    $dir = ClearArchiveByMonth($dir, $month);

$page = isset($_GET['page']) ? (int)$_GET['page'] : 0;

$filesPerPage = 10;


$startIndex = $page * $filesPerPage;

$viewLinks = isset($dir[$startIndex]) ? array_slice($dir, $startIndex, $filesPerPage) : array_slice($dir, 0, $filesPerPage);

$months = array("January" => "Январь", "February" => "Февраль",
"March" => "Март","April" => "Апрель","May" => "Май",
"June" => "Июнь","July" => "Июль","August" => "Август",
"September" => "Сентябрь","October" => "Октябрь",
"November" => "Ноябрь","December" => "Декабрь");

?>

<!DOCTYPE html>
<html lang="ru">
<head>
   <meta charset="utf-8">
   <title>FarpostParser: Listing</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="http://bootswatch.com/flatly/bootstrap.min.css" media="screen">
</head>
<body>

<div class="container" style="text-align: center;">
      <div class="col-md-12" style="text-align: center; margin: 20px;">
<?php
foreach ($months as $key => $value) 
{
?>
        <a href="?month=<?=$key;?>" class="btn btn-success <?=$month == $key ? 'active' : ''; ?>"><?=$value;?></a>
<?php
}

    echo "</div>";  

foreach($viewLinks as $link)
{
?>
    <span style="font-size: 20px;">&#128211;</span><a href="<?=$folder.$link?>"><?=$link?></a><br>
<?php
}
?>
    <div class="pagination">
    <?php
    for ($i = 0; $i < intval(count($dir) / $filesPerPage); $i++)
    {
        ?> 
        <a class="btn btn-success <?=$page == $i ? 'active' : '';?>" href="?month=<?=$month?>&page=<?=$i;?>"><?=($i+1);?></a>
    <?php } 
    ?>
    <br>
<?php
 if(empty($dir))
 {
 ?>
    <div class="alert alert-warning">
        <p>
            Нет файлов для вывода
        </p>
    </div>
<?php
}
?>
    <a class="btn btn-info" href="?month=none&page=0" style="margin-top: 5px;">на главную</a>
    </div>
 </div>
</body>