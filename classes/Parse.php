<?php


class Parse
{
    public function getTypeHouse($dom){
        $fields = $dom->find("span[data-field='constructionStatus']")->text();
        $typeHouse = explode("\n",$fields)[0];

        return $typeHouse;
    }

    public function getAddress($documentDOM, $labelValue, $partOfDistrict = null){

        $labels = $documentDOM->find(".label");
        foreach ($labels as $label){
            $pq = pq($label);
            $value = CreateDocument::fixEncoding($pq->text());

            if ($value == $labelValue) {
                if ($labelValue == "Адрес"){
                    $address = explode(" ", $pq->next()->find("span[data-field='street-district'] a:nth-child(1)")->text());
                    $street = array();
                    $house = array();


                    $startHouse = 0;
                    foreach ($address as $key => $addressData){
                        if ($key === 0 && is_numeric($addressData[0])) {
                            $street[] = $addressData;
                            continue;
                        }

                        if ($startHouse){
                            $house[] = $addressData;
                        } else {
                            if (is_numeric($addressData[0])){
                                $startHouse = 1;
                                $house[] = $addressData;
                            } else {
                                $street[] = $addressData;
                            }
                        }
                    }

                    return array(
                        "street" => implode(" ",$street),
                        "house" => implode(" ",$house),
                    );
                }

                return $pq->next()->find("span[data-field='street-district'] a")->text();
            }
        }

        return "";
    }

    public function getDescription($dom){
        $text = $dom->find("p[data-field='text']")->text();
        if (empty($text)){
            return $dom->find("p[data-field='realtyFeature']")->text();
        }
        else{
            return $text;
        }
    }

    public function getUsernick($dom){
        $users = $dom->find(".userNick']")->text();
        $parts = str_replace("\t","",explode("\n",$users));

        foreach ($parts as $part){
            if (!empty($part)) {
                $user = $part;
                break;
            }
        }

        return $user;

    }
}