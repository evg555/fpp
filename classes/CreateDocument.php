<?php

class CreateDocument{

    private $csv_folder = "";
    private $csv_object = 0;
    public $csv_name = "farpost";
    private $cityName = "vladivostok";

    public function __construct($csv_folder,$cityName){
        $this->csv_folder = $csv_folder;
        $this->cityName = $cityName;
    }

    static public function formatToUtf8($string){
        $string = str_replace(array("\r\n", "\t"), "", trim($string));
        return mb_convert_encoding($string, "WINDOWS-1251","UTF-8");
    }

    static function fixEncoding($string){
        $string = str_replace(array("\r\n", "\t"), "", trim($string));
        $string = mb_convert_encoding($string, "WINDOWS-1252", "UTF-8");
        return mb_convert_encoding($string, "UTF-8", "WINDOWS-1251");
    }

    public function makeCSV($fieldsArray, $pageNumber)
    {
        if(count($fieldsArray) > 1){
            //Получаем заголовки полей
            $fieldKeys = array_keys($fieldsArray);

            if(!is_object($this->csv_object)){
                //Задаем имя файла
                switch ($this->cityName) {
                    case "vladivostok":
                        $csvName = $this->csv_name."-vl-".date("y-m-d").".csv";
                        break;
                    case "khabarovsk":
                        $csvName = $this->csv_name."-kh-".date("y-m-d").".csv";
                        break;
                    default:
                        $csvName = $this->csv_name."-vl-".date("y-m-d").".csv";
                }

                //Удаляем старый файл с таким же именем
                if (is_file($this->csv_folder.$csvName)){
                    unlink($this->csv_folder.$csvName);
                }

                $this->csv_object = new SplFileObject($this->csv_folder.$csvName, 'a+');

                //Пишем заголовки
                $this->csv_object->fputcsv($fieldKeys, ';');
            }

            //Фиксим кодировку

            foreach ($fieldsArray as $apKey => $apartmentRow){
                /*
                //Это поле на странице в чистом win1251
                if($apKey == 'Предложение от' or $apKey == 'Дата публикации') continue;
                */
                foreach ($apartmentRow as $key => $value){
                    $fieldsArray[$apKey][$key] = CreateDocument::fixEncoding($value);
                }
            }

            //Заливаем контент в поля
            for($i=0; $i<count($fieldsArray["ID"]); $i++){
                $out = array();
                foreach($fieldKeys as $v) {
                    $out[] = str_replace("\r\n", "", trim($fieldsArray[$v][$i]));
                }
                $result = $this->csv_object->fputcsv($out, ';');
            }

            if ($result) return true;

            return false;

        }
    }
}