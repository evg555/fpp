<?php

class GetContent
{
    private $cityName = "vladivostok";
    private $headers = "";
    private $context = "";
    private $cookies = "";
    private $cookieFile = "cookie.txt";
    private $log = '';
    private $error = 0;

    public function __construct(LogWrite $log,$cityName)
    {
        $this->cityName = $cityName;
        $this->log = $log;
    }

    public function checkCookies(){
        $this->makeContext();

        if($old = file_get_contents("http://www.farpost.ru/".$this->cityName."/realty/sell_flats/?page=1&ajax=1", false, $this->context))
        {
            //Обновляем сессию раз в два часа
            if(file_exists($this->cookieFile) && (time() - (filemtime($this->cookieFile)) < 14400))
            {
                $this->cookies = file_get_contents($this->cookieFile);
                $this->makeContext();
            }
            else{
                //Создаем куку
                $this->getCookies($http_response_header);
                file_put_contents($this->cookieFile, $this->cookies);
                $this->makeContext();
            }
        }
        else{
            $this->log->logWriter("Не удалось получить СТАРТОВУЮ страницу");
        }
    }

    /* Формирует контекст потока (Маскируемся под браузер)
     * Возвращает resource
    */
    private function makeContext()
    {
        $httpHeaders = array(
            'http' => array(
                "method" => "GET",
                "header"=>"Host: www.farpost.ru\r\n".
                    "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0\r\n".
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n".
                    "Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3\r\n".
                    "Accept-Encoding: gzip, deflate\r\n".$this->cookies.
                    "Connection: keep-alive\r\n"));
        $this->headers = $httpHeaders;
        $this->context = stream_context_create($httpHeaders);
    }

    private function getCookies($http_response_header)
    {
        $cookies_array = array();
        foreach($http_response_header as $s)
        {
            if (preg_match('|^Set-Cookie:\s*([^=]+)=([^;]+);(.+)$|', $s, $parts)){
                if(strstr($parts[2], 'deleted')){
                    continue;
                }
                else {
                    $cookies_array[] = $parts[1] . '=' . $parts[2];
                    break;
                }
            }
        }

        if(!empty($cookies_array)) {
            $this->cookies = 'Cookie: ' . join('; ', $cookies_array) ."\r\n";
        }
    }

    public function getCatalogPage($pageNumber){
        $url = "http://www.farpost.ru/".$this->cityName."/realty/sell_flats/?page=$pageNumber&ajax=1";
        if($page = @file_get_contents($url, false, $this->context)){
            $page = @gzdecode($page) ? gzdecode($page) : $page;
            $page = CreateDocument::formatToUtf8($page);

            if(strpos($page, "id='statusarchive'")){
                $this->log->logWriter("Архив достигнут, завершаю работу.");
                return false;
            }

            return $page;
        } else {
            //Защита от зациклиыания (после 10 несуществующих страниц цикл будет прерван)
            $this->log->logWriter('Не удалось получить страницу каталога, код ошибки:'.$http_response_header[0].' page: #'.$pageNumber);
            $this->error++;

            if ($this->error === 10) return false;

            return true;
        }
    }

    public function getApartment($apartmentURL){
        if($apartment = @file_get_contents("http://www.farpost.ru/".$this->cityName."/".$apartmentURL."?ajax=1", false, $this->context)){
            return $apartment;
        } else {
            $this->log->logWriter('Не удалось получить страницу с апартаментами, код ошибки:'.$http_response_header[0].' URL: '.$apartmentURL);
            return false;
        }
    }
}