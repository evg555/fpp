<?php

class LogWrite{
    private $logFile = "log.txt";

    public function logWriter($message){
        file_put_contents($this->logFile, '['.date("H:m:s d.m.y"). "] ".$message."\r\n", FILE_APPEND | LOCK_EX);
    }
}